import tarfile
import typing

import arpy
from debian import deb822

from dedup.compression import decompress
from dedup.hashing import HashlibLike, hash_file

class MultiHash:
    def __init__(self, *hashes: HashlibLike):
        self.hashes = hashes

    def update(self, data: bytes) -> None:
        for hasher in self.hashes:
            hasher.update(data)


def get_tar_hashes(
    tar: tarfile.TarFile,
    hash_functions: typing.Sequence[typing.Callable[[], HashlibLike]],
) -> typing.Iterator[typing.Tuple[str, int, typing.Dict[str, str]]]:
    """Given a TarFile read all regular files and compute all of the given hash
    functions on each file.
    @param hash_functions: a sequence of parameter-less functions each creating a
            new hashlib-like object
    @returns: an iterable of (filename, filesize, hashes) tuples where
            hashes is a dict mapping hash function names to hash values
    """

    for elem in tar:
        if not elem.isreg(): # excludes hard links as well
            continue
        hasher = MultiHash(*[func() for func in hash_functions])
        extracted = tar.extractfile(elem)
        assert extracted is not None
        hash_file(hasher, extracted)
        hashes = {}
        for hashobj in hasher.hashes:
            hashvalue = hashobj.hexdigest()
            if hashvalue:
                hashes[hashobj.name] = hashvalue
        yield (elem.name, elem.size, hashes)


def opentar(filelike: typing.BinaryIO) -> tarfile.TarFile:
    return tarfile.open(fileobj=filelike, mode="r|", encoding="utf8",
                        errors="surrogateescape")

class DebExtractor:
    "Base class for extracting desired features from a Debian package."

    def __init__(self) -> None:
        self.arstate = "start"

    def process(self, filelike: typing.BinaryIO) -> None:
        """Process a Debian package.
        @param filelike: is a file-like object containing the contents of the
                         Debian packge and can be read once without seeks.
        """
        af = arpy.Archive(fileobj=filelike)
        for member in af:
            self.handle_ar_member(member)
        self.handle_ar_end()

    def handle_ar_member(self, arfiledata: arpy.ArchiveFileData) -> None:
        """Handle an ar archive member of the Debian package.
        If you replace this method, you must also replace handle_ar_end and
        none of the methods handle_debversion, handle_control_tar or
        handle_data_tar are called.
        """
        name = arfiledata.header.name
        if self.arstate == "start":
            if name != b"debian-binary":
                raise ValueError("debian-binary not found")
            version = arfiledata.read()
            self.handle_debversion(version)
            if not version.startswith(b"2."):
                raise ValueError("debian version not recognized")
            self.arstate = "version"
        elif self.arstate == "version":
            if name.startswith(b"control.tar"):
                filelike = decompress(arfiledata, name[11:].decode("ascii"))
                self.handle_control_tar(opentar(filelike))
                self.arstate = "control"
            elif not name.startswith(b"_"):
                raise ValueError("unexpected ar member %r" % name)
        elif self.arstate == "control":
            if name.startswith(b"data.tar"):
                filelike = decompress(arfiledata, name[8:].decode("ascii"))
                self.handle_data_tar(opentar(filelike))
                self.arstate = "data"
            elif not name.startswith(b"_"):
                raise ValueError("unexpected ar member %r" % name)
        else:
            assert self.arstate == "data"

    def handle_ar_end(self) -> None:
        "Handle the end of the ar archive of the Debian package."
        if self.arstate != "data":
            raise ValueError("data.tar not found")

    def handle_debversion(self, version: bytes) -> None:
        """Handle the debian-binary member of the Debian package.
        @param version: The full contents of the ar member.
        """

    def handle_control_tar(self, tarfileobj: tarfile.TarFile) -> None:
        """Handle the control.tar member of the Debian package.
        If you replace this method, none of handle_control_member,
        handle_control_info or handle_control_end are called.
        @param tarfile: is opened for streaming reads
        """
        controlseen = False
        for elem in tarfileobj:
            if elem.isreg():
                name = elem.name
                if name.startswith("./"):
                    name = name[2:]
                extracted = tarfileobj.extractfile(elem)
                assert extracted is not None
                content = extracted.read()
                self.handle_control_member(name, content)
                if name == "control":
                    self.handle_control_info(deb822.Packages(content))
                    controlseen = True
            elif not (elem.isdir() and elem.name == "."):
                raise ValueError("invalid non-file %r found in control.tar" %
                                 elem.name)
        if not controlseen:
            raise ValueError("control missing from control.tar")
        self.handle_control_end()

    def handle_control_member(self, name: str, content: bytes) -> None:
        """Handle a file member of the control.tar member of the Debian package.
        @param name: is the plain member name
        """

    def handle_control_info(self, info: deb822.Packages) -> None:
        """Handle the control member of the control.tar member of the Debian
        package.
        """

    def handle_control_end(self) -> None:
        "Handle the end of the control.tar member of the Debian package."

    def handle_data_tar(self, tarfileobj: tarfile.TarFile) -> None:
        """Handle the data.tar member of the Debian package.
        @param tarfile: is opened for streaming reads
        """
